package com.example.android.BluetoothChat;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity{
	
	EditText un,pass;
	Button login;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		un=(EditText)findViewById(R.id.editText1);
		pass=(EditText)findViewById(R.id.editText2);
		
		login=(Button)findViewById(R.id.button1);
		
		login.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				String uname=un.getText().toString();
				String passwrd=pass.getText().toString();
				
				if(uname.length()>0 && passwrd.length()>0){
					
					SharedPreferences sh=getSharedPreferences("UserDetails", MODE_PRIVATE);
				String UserFromData =	sh.getString("UserName", "");
				String passwrdd =sh.getString("Password", "");
				
				if(uname.equals(UserFromData)){
					if(passwrdd.equals(passwrd)){
						Toast.makeText(getApplicationContext(), "Login success", Toast.LENGTH_SHORT).show();
										
					}else {
						Toast.makeText(getApplicationContext(), "Invalid Password", Toast.LENGTH_SHORT).show();
						
					}
				}else {
					Toast.makeText(getApplicationContext(), "User Name not found!", Toast.LENGTH_SHORT).show();
				}
					
				}else {
					Toast.makeText(getApplicationContext(), "Enter UserName/Password", Toast.LENGTH_SHORT).show();
					
				}
				
			}
		});
		
	}

}
