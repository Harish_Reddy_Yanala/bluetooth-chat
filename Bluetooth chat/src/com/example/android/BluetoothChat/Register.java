package com.example.android.BluetoothChat;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends Activity{
	
	EditText uname, pass1,pass2;
	Button reg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
		uname=(EditText)findViewById(R.id.editText1);
		pass1=(EditText)findViewById(R.id.editText2);
		pass2=(EditText)findViewById(R.id.editText3);
		
		reg=(Button)findViewById(R.id.button1);
		
		
		reg.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				String unames=uname.getText().toString();
				String pass11=pass1.getText().toString();
				String pass22=pass2.getText().toString();
				
				if(unames.length()>0 && pass11.length()>0 && pass22.length()>0){
					
					if(pass11.equals(pass22)){
						
						SharedPreferences sh=getSharedPreferences("UserDetails", MODE_PRIVATE);
						Editor ed=sh.edit();
						ed.putString("UserName", unames);
						ed.putString("Password", pass11);
						ed.commit();
						
						
						
		Toast.makeText(getApplicationContext(), "Register Success!", Toast.LENGTH_SHORT).show();
						
		
				startActivity(new Intent(getApplicationContext(),Login.class));
					}else {
					Toast.makeText(getApplicationContext(), "Password Not Match", Toast.LENGTH_SHORT).show();
											
					}
					
				}else {
					Toast.makeText(getApplicationContext(), "All fields are mandatory", Toast.LENGTH_SHORT).show();
				}
				
				
			}
		});
		
	}

}
